/**
 * Gulp configuration for generate vector icons from the svg
 */

const vendor_name = 'Amasty',
    module_name = 'Module_Name', // Type name of module there
    styles_folder = 'frontend/web/css/source/module/'; // Type path to styles folder there

let gulp = require('gulp'),
    iconfont = require('gulp-iconfont'),
    iconfontCSS = require('gulp-iconfont-css'),
    env = {
        styles_path: () => 'app/code/' + vendor_name + '/' + module_name + '/view/' + styles_folder,
        media_short_path: () => 'pub/media/' + vendor_name.toLowerCase() + '/' + module_name.toLowerCase() + '/',
        media_path: () => 'app/code/' + vendor_name + '/' + module_name + '/' + env.media_short_path()
    },
    icons = {
        name: module_name.toLowerCase() + '-icon',
        css: {
            font_path: '/' + env.media_short_path(),
            target_path: '../../../../../../../../' + env.styles_path() + '_icons.less', // Only relative path
            template: env.styles_path() + '_icons_template.txt'
        },
        src: env.styles_path() + 'icons/',
    };

// Move Static Files into Magento's media folder
function moveStatic() {
    gulp.src(env.media_path() + '**.*').pipe(gulp.dest(env.media_short_path()));
}

// Icons font generation from the SVG's
gulp.task('iconfont', function () {
    gulp.src([icons.src + '*.svg'])
        .pipe(iconfontCSS({
            fontName: icons.name,
            targetPath: icons.css.target_path,
            fontPath: icons.css.font_path,
            path: icons.css.template
        }))
        .pipe(iconfont({
            fontName: icons.name,
            formats: ['ttf', 'eot', 'woff', 'woff2'], // Remove woff2 if you get an ext error on compile
            prependUnicode: true,
            normalize: true,
            fontHeight: 1000
        }))
        .pipe(gulp.dest(env.media_path()))
        .on('end', moveStatic)
});
